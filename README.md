# Lab Logger with JUnit
## CSI3471 Lab
### Xingan Wan
1. Log in file are different from log in console, this is because in setup (“logger. properties”), the console handler level set to “INFO”, file handler level set to “ALL”, which means console will only display info level log while file will have everything.

2. This line comes from org.junit.jupiter.engine.execution.ConditionEvaluator.

3. Assertions.assertThrows asserts that execution of the supplied Timer throws an exception of the TimerException and returns the exception.

4.  1. serialVersionUID is the serialization runtime associates with each serializable class a version number. We need it because if we don't explicitly specify serialVersionUID, a value is generated automatically, but that's brittle because it's compiler implementation dependent. Therefore, to guarantee a consistent serialVersionUID value across different java compiler implementations, a serializable class must declare an explicit serialVersionUID value.
    2. If not override constructors, when we create a TimerException, it will create its parent – Exception, but what we need is a TimerException instance.
    3. For other methods they already in Parent class, we can call them directly from TimerException without override them. This is polymorphism. 

5. A static block in Java is a block of code that is executed at the time of loading a class for use in a Java application. It is used for initializing static Class members in general — and is also known as a 'Static Initializer'. Here when loading the timer class, it sets logger’s configuration which detailed in a file.

6. It is Markdown syntax for formatting text, we can use it in any pull request's descriptions or comments, or in README files (if they have the .md file extension), and if bitbucket repository contains a README.md file at the root level, Bitbucket Server displays its contents on the repository's Source page.

7. This test failing because it catches an unexpected exception type thrown: ==> expected: <edu.baylor.ecs.si.TimerException> but was: <java.lang.NullPointerException>. In method timeMe(), the if block which check if time less than 0  is surround by a try block but does not throw with proper exception, to fix it, we can simply move the if block before the try block and will pass all tests.

8. After debug the Junit test, I found it checks for exceptions, when it is a NullPointerException, throwable collector does not accept it, it must receive a TimerException.

9. ![Alt text](/junit.png "JUnit5 plugin run")

10. ![Alt text](/maven.png "Maven test run")

11.	The TimerException is a subclass of Exception, NullPointerException is a RuntimeException.
